//: Playground - noun: a place where people can play

import UIKit

/*
    Variables
*/
var name:String = "Vincent"
print("Hello " + name + ".")

var myInt:Int = 9
myInt = myInt * 2
myInt = myInt / 4 // note it rounds down
print("The value of myInt is /(myInt)")

var myDouble:Double = 8.4 // double cannot perform operation with int
print(myDouble * Double(myInt))

var isMale:Bool = true


// Challenge, create a double and a interger, multiply them and produce a string
var a:Double = 7.6
var b:Int = 3

print("Product of \(a) and \(b) is \(a * Double(b))")




