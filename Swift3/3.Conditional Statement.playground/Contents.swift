//: Playground - noun: a place where people can play

import UIKit

/*
    Conditional Statement (if statements)
*/

var age = 20

if age >= 18 {
    print("You are legal!")
} else {
    print("Too young! Come back in a few years.")
}

var name = "Vincent"
if name == "Vincent" { // difference between = and ==
    print("Welcome, master Wei")
} else {
    print("Access denied!")
}

var friend = "Dara"
if name == "Vincent" && friend == "Dara"{
    print("Get a room!")
}

var cofounder = "Frank"
if name == "Vincent" || cofounder == "Elvis" {
    print("Welcome, access granted. How may I help?")
}

var isMale = true
if isMale {
    print("Aim well in the bathroom please!")
}


// Challenge: set up a username and password variable and check if they are correct, allow the user in and print access granted. If either is incorrect, tell the user which is incorrect.

var username = "vinba"
var password = "123456"

if username == "vinba" && password == "123456" {
    print("Access Granted!")
} else if username == "vinba" {
    print("Incorrect password")
} else if password == "123456" {
    print("Incorrect username")
} else {
    print("Incorrect username and password")
}


