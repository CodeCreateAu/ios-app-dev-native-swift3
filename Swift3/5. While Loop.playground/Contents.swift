//: Playground - noun: a place where people can play

import UIKit

/*
    While Loop
*/

var i = 0
while i < 10 {
    print(i)
    i += 1
}


// Challenge: can you use a while loop to display the first 10 of the 5's multiplication table

var j = 1
while j <= 10 {
    print (i*5)
    j += 1
}


// A way to loop through array with while loop

var arr = [8,3,1,2,6,4,8,9]

var index = 0
while index < arr.count {
    print (arr[index])
    index += 1
}

