//: Playground - noun: a place where people can play

import UIKit

//: Playground - noun: a place where people can play


/*
    Array
*/

var array = [17, 25, 13, 47]

print(array[2])

print(array.count)

array.append(56)

array.remove(at: 3)

print(array)

array.sort()


// Challenge: Create an array of 3 double, removie the second double and append a new double which is a multiplication of the first and the third element previously

var myArray = [3.87, 7.1, 8.9]

myArray.remove(at: 1)

myArray.append(myArray[0] * myArray[1])



/*
    Dictionaries
*/

var dictionary = ["computer": "something to play Call Of Duty on", "coffee": "best drink ever"]

print(dictionary["coffee"]!)

print(dictionary.count)

dictionary["pen"] = "Old fashioned writing implement"

dictionary.removeValue(forKey: "computer")

print(dictionary)

// Challenge

var menu = ["pizza": 10.99, "ice cream": 4.99, "salad": 7.99]

var cost = menu["pizza"]! + menu["ice cream"]! + menu["salad"]!

//var totalCost = menu["pizza"]! + menu["ice cream"]! + menu["salad"]
//
//print("The total cost of the tree items is \(totalCost)")





