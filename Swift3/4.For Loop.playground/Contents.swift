//: Playground - noun: a place where people can play

import UIKit

/*
    For Loop
*/

for i in 1...10 {
    print (i)
}


var arr = [8, 3, 9, 10]
for num in arr {
    print(num)
}

for (index, value) in arr.enumerate() { // enumerate allows us to get index and value
    print (value)
    arr[index] += 1
}
print(arr)


// Challenge: creating an array, take the array, half all of the value in the array
for (index, value) in arr.enumerate() {
    arr[index] /= 2
}
print(arr)

