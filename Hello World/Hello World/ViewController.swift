//
//  ViewController.swift
//  Hello World
//
//  Created by Vincent Wei on 1/06/2016.
//  Copyright © 2016 minibolt. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var greetLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func updateGreeting(_ sender: AnyObject) {
        
        greetLabel.text = "Hi \(nameTextField.text!)!"
        
    }

}

